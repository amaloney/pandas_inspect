from functools import wraps

from pandas.api.extensions import register_series_accessor


def register_series_method(method):

    def inner(*args, **kwargs):

        class AccessorMethod:

            __doc__ = method.__doc__

            def __init__(self, df):
                self.df = df

            @wraps(method)
            def __call__(self, *args, **kwargs):
                return method(self.df, *args, **kwargs)

        register_series_accessor(method.__name__)(AccessorMethod)
        return method

    return inner()
